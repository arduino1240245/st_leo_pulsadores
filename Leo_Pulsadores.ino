
//Hecho por Federico Capdeville
//Probado con el Blue Pill (STM32F103C8T6)

//Con un pulsador prendo un led, con el otro lo apago.

#define LED PB12
#define PUL_ON PA8
#define PUL_OFF PA9

int pul_on = 0, pul_off = 0;

void setup() 
{
  pinMode(LED, OUTPUT);      //Coloco un led en la pata PB12
  pinMode(PUL_ON, INPUT);        //Coloco un pulsador
  pinMode(PUL_OFF, INPUT);       //Coloco un pulsador
}

void loop() 
{
  pul_on = digitalRead(PUL_ON);
  pul_off = digitalRead(PUL_OFF);
  
  if (pul_on == LOW)
    digitalWrite(LED, HIGH);

  if (pul_off == LOW)
    digitalWrite(LED, LOW);
}
